# Compilation

The input file `davidjhshih.raw.bib` needs to be created manullay:

1. Retrieve PMIDs of publications from MyNCBI.
2. Convert PMIDs to bibtex using [pm2bib](https://github.com/djhshih/pm2bib).
3. Make manual corrections:
    * Correct UTF-8 alpha characters to $\alpha$.
    * Rename entries with the same id (e.g. northcott12)

Avoid using ORCID for generating bibtex files,
because it seems to export really poorly formatted files!

Then,

```
make
```

Output file is `publication-list_davidjhshih.pdf`.
