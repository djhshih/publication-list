bib = davidjhshih
tex = publication-list_davidjhshih

publication-list_davidjhshih.pdf: $(tex).tex $(bib).bib
	xelatex $(tex)
	bibtex $(tex)
	xelatex $(tex)
	xelatex $(tex)

$(bib).bib: $(bib).raw.bib
	cat $< extra/*.bib | \
		sed 's/Shih/\\underline{\\textbf{Shih}}/g' > $@

$(bib).raw.bib: $(bib).pmid
	pm2bib -f $(bib).pmid > $(bib).raw.bib

clean:
	rm -rf *.aux *.log *.bbl *.blg $(tex).pdf $(bib).bib

